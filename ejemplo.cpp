/* 
 * $ g++ -std=c++11 valor-reference.cpp -o valor-reference
 */

#include <iostream>
using namespace std;

/* recibe parámetro por valor (copia). */
int cubo(int i) {
    i = i * i * i;
    return (i);
}

/* recibe parámetro por referencia. */
void f1(int &r) {
    r = r + r;
}

/* */
int main(int argc, char **argv) {
    int i;
    
    // paso por valor de la variable "i", pasa una copia.
    for (i=1; i <= 5; i++) {
        cout << "i: " << i << " cubo: " << cubo(i) << endl;
    }
    
    cout << "......................" << endl;
    
    // paso por referencia de la variable "i".
    for (i=1; i < 5; i++) {
        cout << "i: " << i << " ";
        f1(i);
        cout << "i: " << i << endl;
    }
    
    return 0;
}

