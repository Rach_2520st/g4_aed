#include <iostream>
#include <fstream>
#include "Arbol.h"

using namespace std;

#ifndef GRAFO_H
#define GRAFO_H

class Grafo
{
public:
	//constructor
	Grafo(Nodo *nodo);
	//metodos de la clase

	void recorrer_arbol(Nodo *, ofstream &);
	
};
#endif